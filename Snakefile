from os.path import join

configfile: "config.yaml"

## Generate reference index files
include: "./rules/bwa/BWAIndex"
include: "./rules/picard/CreateDict"
include: "./rules/samtools/SamFaidx"

## Alignment
include: "./rules/bwa/BWAMem"
include: "./rules/picard/SamToBam"
include: "./rules/picard/FixMates"
include: "./rules/picard/MarkDups"
include: "./rules/gatk/TargetIndels"
include: "./rules/gatk/RealignIndels"

## Alignment metrics, uncovered regions and regions <10x filtered from variant callling
include: "./rules/gatk/CoverageStats"
include: "./rules/samtools/FlagStat"

## Variant calling, filtering and combining - SNVs and indels < 60bp
include: "./rules/gatk/HaplotypeCaller"
include: "./rules/gatk/HapCallFilter"
include: "./rules/gatk/HapCallCombine"

## Variant calling - SVs
include: "./rules/gridss/Gridss"

bwa = config["bwa"]
date = config["date"]
fa = config["reference_genome"]
gatk = config["gatk"]
insert_size = config["insert_size"]
java = config["java"]
picard = config["picard"]
project_id = config["project_id"]
python = config["python"]
reference_dir = config["reference_dir"]
reference_name = config["reference_name"]
samples = config["samples"]
samtools = config["samtools"]
tempfolder = config["tempfolder"]
threads = config["threads"]
workbatch = config["workbatch"]
gridss = config["gridss"]
gridss_jar = config["gridss_jar"]

rule all:
    input:
        #expand("{workbatch}/metrics/coverage_stats/{sample}_coverage.sample_summary", workbatch=workbatch, sample=samples),
        #expand("{workbatch}/metrics/{sample}.flagstats", workbatch=workbatch, sample=samples),
        #expand("{workbatch}/variants/haplotypecaller/combined/HapCallCombined.vcf", workbatch=workbatch),
        expand("{workbatch}/variants/gridss/raw/gridss_raw.vcf", workbatch=workbatch),
