## VariantDetection pipeline 

Detects SNVs and short indels (~<60bp) using GATK HaplotypeCaller.  
Tested using Illumina HiSeq 2500 125bp paired-end reads.

# Useage overview

*   Install prerequisite software detailed below
*   Clone pipeline to local machine e.g.:  

     ```
     git clone git@git.exeter.ac.uk:sc651/variantdetection.git
     ```

*   Add local variables to config.yaml
*   An example cluster execution command would be:

     ```
     nohup snakemake -j 100 --cluster-config cluster.json --cluster "qsub -V -e {cluster.log} -o {cluster.log} -d . 
     -q pq -l walltime={cluster.time} -A {cluster.pid} -l nodes={cluster.nodes}:ppn={cluster.ppn} -m e -M {cluster.email} 
     -N {cluster.name}" &
     ```

*   For non distributed execution an example command would be:

     ```
     nohup snakemake --cores 4 2>&1 $
     ```

# Description
The following is a Snakemake pipeline constructed for variant calling in haploid fungal genomes from filtered short read paired-
end NGS sequencing.  
Briefly it encorporates:
* Preparation of reference meta-files (dict, fai, etc...)
* Alignment with bwa-mem
* BAM processing (Picard)
* Local realignment of variable regions (GATK)
* Variant calling with GATK HaplotypeCaller (ploidy 1)
* Variant filtering based on read depth and proportion of alternative allele calls of total.
* FASTQ metrics (FastQC), Alignemnt metrics (samtools flagstat, GATK DepthofCoverage)

**Note** Variants are called on each sample individually and combined after filtering.

## Prerequisites

The following software versions were used when the pipeline was constructed.
More recent software versions may also enable pipeline execution but have not been tested.

- [Snakemake 4.2.0](https://snakemake.readthedocs.io/en/stable/index.html)
- [Python 3.5.1](https://www.python.org/) [PyYAML (3.12)]
- [Java 1.8.0_92](https://www.oracle.com/technetwork/java/javase/overview/index.html)
- [GATK](https://software.broadinstitute.org/gatk/)
- [Picard 2.12.1](http://broadinstitute.github.io/picard/)
- [Samtools 1.3.1](http://www.htslib.org/doc/samtools.html)
- [BWA 0.7.15-r1140](http://bio-bwa.sourceforge.net/)
- [snpEff 4.3r] (http://snpeff.sourceforge.net/)

Includes cluster configuration file. 
Snakefile endpoints defaults to all outputs.
Deletes intemediate files.
Complete config.yaml

## Inputs


